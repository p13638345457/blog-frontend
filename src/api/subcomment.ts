import http from "../utils/http";
import {MyApiResponse} from "../utils/types";

export const getUserNameBySubCommentId = async (subCommentId: number) => {
    try {
        const response = await http.get<MyApiResponse<string>>(`/getUsernameBySubCommentId`, {params: {subCommentId: subCommentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching parent username:', error);
        return '未知用户';
    }
}