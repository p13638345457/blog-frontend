import http from "../utils/http";
import {MyApiResponse, SubCommentType} from "../utils/types";

export const getSubCommentCountByCommentId = async (commentId: number)=> {
    try {
        const response = await http.get<MyApiResponse<number>>(`/getSubCommentsByCommentId`, {params: {commentId: commentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching subCommentCount:', error);
        return 0;
    }
}

export const getSubCommentsByCommentId = async (commentId: number) => {
    try {
        const response = await http.get<MyApiResponse<SubCommentType[]>>(`/getSubCommentByCommentId`, {params: {commentId: commentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching subComments:', error);
        return [];
    }
}

