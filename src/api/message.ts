import http from "../utils/http";
import {MessageType} from "../utils/types";

export const getMessagesByUserId = async (userId: number) => {
    try {
        const response = await http.get(`/getMessageByUserId`, {params: {userId: userId}});
        return response.data;
    } catch (error) {
        console.log(error);
        return [] as MessageType[];
    }
}