import http from "../utils/http";
import {MyApiResponse} from "../utils/types";

export const getLikesByCommentId = async (commentId: number) => {
    try {
        const response = await http.get<MyApiResponse<number>>(`/getLikesByCommentId`, {params: {commentId: commentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching likes:', error);
        return 0;
    }
}

export const getLikeStatusByCommentId = async (commentId: number) => {
    try {
        const response = await http.get<MyApiResponse<boolean>>(`/isCommentLiked`, {params: {commentId: commentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching status:', error);
        return false;
    }
}