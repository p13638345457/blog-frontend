import http from "../utils/http";
import {MyApiResponse} from "../utils/types";

export const getLikesByPostId = async (postId: number) => {
    try {
        const response = await http.get<MyApiResponse<number>>(`/getLikesByPostId`, {params: {postId: postId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching likes:', error);
        return 0;
    }
}

export const getLikeStatusByPostId = async (postId: number) => {
    try {
        const response = await http.get<MyApiResponse<boolean>>(`/isPostLiked`, {params: {postId: postId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching status:', error);
        return false;
    }
}