// api/user
import http from "../utils/http";
import {MessageType, MyApiResponse, PostType, UserType} from "../utils/types";
import {noticeError} from "../utils/notice";
import {useRouter} from "vue-router";

// 设置一个默认的用户对象
const defaultUser: UserType = {
    userId: 0,
    username: '未知用户',
    email: 'default@example.com',
    role: 'guest',
    createdAt: '',
    updatedAt: '',
    icon: 'default_icon_url', // 假设有一个默认的头像URL
    sign: '这是一个默认签名'
};

export const getUserNameByUserID = async (userId: number) => {
    try {
        const response = await http.get<MyApiResponse<string>>('/getUsernameByUserId', {params: {userId: userId}});
        return response.data.data || '未知用户';
    } catch (error) {
        console.error('获取用户名失败:', error);
        return '未知用户';
    }
}

export const getUserByUserId = async (userId: number) => {
    try {
        const response = await http.get<MyApiResponse<UserType>>(`/getUserByUserId`, {params: {userId: userId}});
        return response.data.data;
    } catch (error) {
        console.error("获取用户失败:", error);
        return defaultUser as UserType;
    }
}

export const getPostsByUserId = async (userId: number) => {
    try {
        const response = await http.get<MyApiResponse<PostType[]>>(`/getPostsByUserId`, {params: {userId: userId}});
        return response.data.data;
    } catch (error) {
        console.error("获取用户的帖子失败:", error);
        return [] as PostType[];
    }
}

export const getMessageByUserId = async (userId: number) => {
    try {
        const response = await http.get<MyApiResponse<MessageType[]>>(`/getMessageByUserId`, {params: {userId: userId}});
        return response.data.data;
    } catch (error) {
        console.error("获取用户的留言失败:", error);
        return [] as MessageType[];
    }
}

export const getCurrentUser = async () => {
    try {
        const response = await http.get<MyApiResponse<UserType>>(`/getCurrentUser`);
        return response.data.data;
    } catch (error) {
        console.log("获取用户信息失败");
        noticeError("信息检查出错，将引导您重新登陆");
        const router = useRouter();
        await router.push("/login");
        return defaultUser;
    }
}

export const updateUser = async (user: UserType) => {
    try {
        await http.put(`/updateUser`, user);
    } catch (error) {
        console.log("更新信息失败");
    }
}