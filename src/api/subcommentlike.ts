import http from "../utils/http";
import {MyApiResponse} from "../utils/types";

export const getLikesBySubCommentId = async (subCommentId: number) => {
    try {
        const response = await http.get<MyApiResponse<number>>(`/getLikesBySubCommentId`, {params: {subCommentId: subCommentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching likes:', error);
        return 0;
    }
}

export const getLikeStatusBySubCommentId = async (subCommentId: number) => {
    try {
        const response = await http.get<MyApiResponse<boolean>>(`/isSubCommentLiked`, {params: {subCommentId: subCommentId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching like status:', error);
        return false;
    }
}