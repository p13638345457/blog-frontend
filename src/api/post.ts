import http from "../utils/http";
import {CommentType, MyApiResponse, PostType} from "../utils/types";

const defaultPost: PostType = {} as PostType;

export const getCommentsByPostId = async (postId: number) => {
    try {
        const response = await http.get<MyApiResponse<CommentType[]>>(`/getCommentByPostId`, {params: {postId: postId}});
        return response.data.data || [];
    } catch (error) {
        console.error('Error fetching comments:', error);  // 在控制台记录错误
        return [];
    }
}

export const getCommentCountByPostId = async (postId: number) => {
    try {
        const response = await http.get<MyApiResponse<number>>(`/getCommentsByPostId`, {params: {postId: postId}});
        return response.data.data;
    } catch (error) {
        console.error('Error fetching comment count:', error);  // 在控制台记录错误
        return 0;
    }
}

export const getPostByPostId = async (postId: number) => {
    try {
        const response = await http.get<MyApiResponse<PostType>>(`/getPostByPostId`, {params: {postId: postId}});
        console.log(response.data.data);
        return response.data.data;
    } catch (error) {
        console.error('Error fetching comment count:', error);  // 在控制台记录错误
        return defaultPost;
    }
}