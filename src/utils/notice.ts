// 文件: useNotification.ts
import {ElNotification} from 'element-plus';

// 定义通知类型
type NotificationType = 'success' | 'warning' | 'info' | 'error';

interface NotificationOptions {
    title: string;
    message: string;
    type: NotificationType;
    duration: number;
}

// 封装的通知函数
function useNotification({title, message, type, duration}: NotificationOptions) {
    return ElNotification({
        title: title,
        message: message,
        type: type,
        duration: duration
    });
}

export function noticeSuccess(message: string) {
    return useNotification({title: '成功', message, type: 'success', duration: 5000});
}

export function noticeWarning(message: string) {
    return useNotification({title: '警告', message, type: 'warning', duration: 5000});
}

export function noticeInfo(message: string) {
    return useNotification({title: '信息', message, type: 'info', duration: 5000});
}

export function noticeError(message: string) {
    return useNotification({title: '错误', message, type: 'error', duration: 5000});
}
