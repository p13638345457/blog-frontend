// src/utils/auth.ts
import http from "./http";

// 获取Token
export const getJwt = (): string | null => {
    return localStorage.getItem('jwt');
};

// 保存Token
export const saveJwt = (token: string): void => {
    localStorage.setItem('jwt', token);
};

// 清除Token
export const clearJwt = (): void => {
    localStorage.removeItem('jwt');
};

// 刷新Token
export const refreshJwt = async (): Promise<string> => {
    const token = getJwt();
    if (!token) throw new Error('No token available.');

    try {
        const response = await http.post('http://localhost:8080/refreshJwt');
        const newToken = response.data.data;
        saveJwt(newToken);
        return newToken;
    } catch (error) {
        clearJwt(); // 如果刷新失败，清除无效的JWT
        throw new Error('Failed to refresh token.');
    }
};
