// /utils/types
// 接受后端返回
export interface MyApiResponse<T> {
    message: string;
    data: T; // 泛型类型，可根据使用时确定具体类型
}

export interface PostType {
    postId: number;
    title: string;
    userId: number;
    createdAt: string;
    updatedAt: string;
    content: string;
}

export interface Post {
    title: string;
    content: string;
}

export interface CommentType {
    commentId: number;
    userId: number;
    content: string;
    createdAt: string;
}

export interface SubCommentType {
    subCommentId: number;
    commentId: number;
    userId: number;
    parentSubCommentId: number | null;
    content: string;
    createdAt: string;
}

export interface UserType {
    userId: number;
    username: string;
    email: string;
    role: string;
    createdAt: string;
    updatedAt: string;
    icon: string;
    sign: string;
}

export interface MessageType {
    messageId: number;
    formUserId: number;
    toUserId: number;
    content: string;
    createdAt: string;
    parentId: number;
}