import {onMounted, ref} from 'vue'

const isDark = ref(window.matchMedia('(prefers-color-scheme: dark)').matches)

export function useDarkMode() {
    function toggleDarkMode() {
        isDark.value = !isDark.value
        document.body.classList.toggle('dark', isDark.value)
    }

    // 在组件挂载时设置暗黑模式
    onMounted(() => {
        document.body.classList.toggle('dark', isDark.value)
    })

    return {
        isDark,
        toggleDarkMode,
    }
}
