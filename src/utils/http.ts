// src/utils/http.ts
import axios from 'axios';
import { getJwt} from "./auth";

const http = axios.create({
    baseURL: 'http://localhost:8080',
    timeout: 10000,
});

// 请求拦截器，让所有请求携带jwt
http.interceptors.request.use(
    config => {
        const token = getJwt();
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => Promise.reject(error)
);

export default http;
