// 引入 Vue Router
import {createRouter, createWebHistory} from 'vue-router';

// 定义路由规则
const routes = [
    {path: '/login', component: () => import('../components/user/Login.vue')},
    {path: '/register', component: () => import('../components/user/Register.vue')},
    {path: '/home', component: () => import('../components/home/BlogHome.vue')},
    {path: '/', component: () => import('../components/home/BlogHome.vue')},
    {path: '/createPost', component: () => import('../components/post/CreatePost.vue')},
    {
        path: '/searchResult',
        name: 'SearchResult',
        component: () => import('../components/common/SearchResult.vue'),
        props: (route: any) => ({ query: route.query.searchQuery }) // 可选：直接将查询参数作为 props 传递给组件
    },
    {
        path: '/postDetail/:postId',
        name: 'PostDetail',
        component : () => import('../components/post/PostDetail.vue')
    },
    {
        path: '/editUserInfo',
        name: 'EditUserInfo',
        component: () => import('../components/user/UserInfoManage.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {path:'/userHome/:userId', name: 'UserHome', component: () => import('../components/user/UserHome.vue')},
    {path: '/forgotPassword', name: 'ForgetPassword', component: () => import('../components/user/ForgetPassword.vue')},
    {path: '/resetPassword', name: 'ResetPassword', component: () => import('../components/user/ResetPassword.vue')},
    {path: '/changeMail', name: 'ChangeEmail', component: () => import('../components/user/ChangeEmail.vue')}
];

// 创建 router 实例
const router = createRouter({
    // 使用 HTML5 历史模式
    history: createWebHistory(),
    routes: routes,
});

export default router;
