import 'marked';
declare module 'marked' {
    namespace marked {
        interface MarkedOptions {
            highlight?: (code: string, lang: string) => string;
        }
    }
}